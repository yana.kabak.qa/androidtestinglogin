package com.example.myapplication.robolectric.gallery;

import com.example.myapplication.pages.Pages;
import com.example.myapplication.ui.gallery.ScrollingActivity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.Shadows;
import org.robolectric.shadows.ShadowActivity;

import static org.junit.Assert.assertTrue;

@RunWith(RobolectricTestRunner.class)
public class BackButtonInScrollingActivityTest {

    private ScrollingActivity activity;
    private ShadowActivity shadowActivity;

    @Before
    public void setup() {
        activity = Robolectric.buildActivity(ScrollingActivity.class)
                .create()
                .resume()
                .get();
    }

    @Test
    public void testBackButton(){
        shadowActivity = Shadows.shadowOf(activity);
        Pages.galleryPage().clickOnBackButton(shadowActivity);
        assertTrue(activity.isFinishing());
    }
}
