package com.example.myapplication.robolectric.slideshow;

import com.example.myapplication.pages.Pages;
import com.example.myapplication.ui.slideshow.SlideActivity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.Shadows;
import org.robolectric.shadows.ShadowActivity;

import static org.junit.Assert.assertTrue;

@RunWith(RobolectricTestRunner.class)
public class BackButtonInSlideActivityTest {

    private SlideActivity activity;

    @Before
    public void setup() {
        activity = Robolectric.buildActivity(SlideActivity.class)
                .create()
                .resume()
                .get();
    }

    @Test
    public void testBackButton(){
        ShadowActivity shadowActivity = Shadows.shadowOf(activity);
        Pages.slideshowPage().clickOnBackButton(shadowActivity);
        assertTrue(activity.isFinishing());
    }
}
