package com.example.myapplication.robolectric.gallery;

import com.example.myapplication.pages.Pages;
import com.example.myapplication.ui.gallery.ScrollingActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import static org.junit.Assert.assertTrue;

@RunWith(RobolectricTestRunner.class)
public class AddNewImageButtonInScrollingActivityTest {

    private ScrollingActivity activity;
    private FloatingActionButton button;

    @Before
    public void setup() {
        activity = Robolectric.buildActivity(ScrollingActivity.class)
                .create()
                .resume()
                .get();
        button = (FloatingActionButton) Pages.galleryPage().getButtonActivity(activity);
    }

    @Test
    public void testAddNewImageButton(){
        Pages.galleryPage().clickOnNewImageButton(button);
        assertTrue(button.isEnabled());
    }

}
