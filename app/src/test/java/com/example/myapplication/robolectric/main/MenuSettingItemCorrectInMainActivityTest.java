package com.example.myapplication.robolectric.main;

import android.content.Intent;

import com.example.myapplication.R;
import com.example.myapplication.pages.Pages;
import com.example.myapplication.ui.main.MainActivity;
import com.example.myapplication.ui.settings.SettingsActivity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.Shadows;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowIntent;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@RunWith(RobolectricTestRunner.class)
public class MenuSettingItemCorrectInMainActivityTest {
    private MainActivity activity;

    @Before
    public void setup() {
        activity = Robolectric.buildActivity(MainActivity.class)
                .create()
                .resume()
                .get();
    }

    @Test
    public void MenuSettingItemCorrect() {
        ShadowActivity shadowActivity = Shadows.shadowOf(activity);
        Pages.mainPage().clickOnMenuItemSetting(shadowActivity);
        Intent startedIntent = shadowActivity.getNextStartedActivity();
        ShadowIntent shadowIntent = Shadows.shadowOf(startedIntent);
        assertThat(shadowIntent.getIntentClass().getName(), equalTo(SettingsActivity.class.getName()));
    }
}
