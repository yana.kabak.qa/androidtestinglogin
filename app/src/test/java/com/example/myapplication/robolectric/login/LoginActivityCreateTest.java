package com.example.myapplication.robolectric.login;

import com.example.myapplication.ui.login.LoginActivity;

import junit.framework.TestCase;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.*;

@RunWith(RobolectricTestRunner.class)
public class LoginActivityCreateTest extends TestCase {

    private LoginActivity loginActivity;

    @Before
    public void setLoginActivity(){
        loginActivity = Robolectric.buildActivity(LoginActivity.class)
                .create()
                .resume()
                .get();
    }

    @Test
    public void testOnCreate(){ Assert.assertNotNull(loginActivity); }
}