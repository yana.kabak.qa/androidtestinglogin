package com.example.myapplication.robolectric.main;

import android.widget.TextView;

import com.example.myapplication.pages.Pages;
import com.example.myapplication.ui.main.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.utils.Constants;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(RobolectricTestRunner.class)
public class ValidateHomeTextInMainActivityTest {

    private MainActivity activity;

    @Before
    public void setup() {
        activity = Robolectric.buildActivity(MainActivity.class)
                .create()
                .resume()
                .get();
    }

    @Test
    public void validateTextHomeViewContent() {
        TextView tvWelcome = Pages.mainPage().getWelcomeText(activity);
        assertNotNull("TextView could not be found", tvWelcome);
        assertEquals("TextView contains incorrect text", Constants.SET_MESSAGE_IN_HOME_PAGE, tvWelcome.getText().toString());
    }
}
