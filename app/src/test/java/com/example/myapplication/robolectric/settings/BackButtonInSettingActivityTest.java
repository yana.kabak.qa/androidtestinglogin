package com.example.myapplication.robolectric.settings;

import com.example.myapplication.pages.Pages;
import com.example.myapplication.ui.settings.SettingsActivity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.Shadows;
import org.robolectric.shadows.ShadowActivity;

import static org.junit.Assert.assertTrue;

@RunWith(RobolectricTestRunner.class)
public class BackButtonInSettingActivityTest {

    private SettingsActivity activity;

    @Before
    public void setLoginActivity(){
        activity = Robolectric.buildActivity(SettingsActivity.class)
                .create()
                .resume()
                .get();
    }

    @Test
    public void testBackButton(){
        ShadowActivity shadowActivity = Shadows.shadowOf(activity);
        Pages.settingsPage().clickOnBackButton(shadowActivity);
        assertTrue(activity.isFinishing());
    }

}
