package com.example.myapplication.robolectric.login;

import android.widget.Button;

import com.example.myapplication.pages.Pages;
import com.example.myapplication.ui.login.LoginActivity;
import com.example.myapplication.utils.Constants;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.shadows.ShadowToast;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertNotNull;

@RunWith(RobolectricTestRunner.class)
public class FailedLoginMessageTest {

    private LoginActivity loginActivity;
    private Button view;

    @Before
    public void setLoginActivity(){
        loginActivity = Robolectric.buildActivity(LoginActivity.class)
                .create()
                .resume()
                .get();
    }

    @Test
    public void buttonClickShouldShowMessage()
    {
        view = Pages.loginPage().getLoginButton(loginActivity);
        assertNotNull(view);
        Pages.loginPage().clickOnLoginButton(view);
        assertThat(ShadowToast.getTextOfLatestToast(), equalTo(Constants.FAILED_LOGIN) );
    }
}
