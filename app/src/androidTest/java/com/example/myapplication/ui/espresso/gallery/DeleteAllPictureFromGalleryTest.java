package com.example.myapplication.ui.espresso.gallery;

import com.example.myapplication.pages.Pages;
import com.example.myapplication.utils.RecyclerViewItemCountAssertion;
import com.example.myapplication.ui.login.LoginActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import androidx.test.espresso.Espresso;
import androidx.test.ext.junit.rules.ActivityScenarioRule;

public class DeleteAllPictureFromGalleryTest {

    @Rule
    public ActivityScenarioRule<LoginActivity> mActivityRule = new ActivityScenarioRule<>(
            LoginActivity.class);

    @Before
    public void setUp() {
    }

    @Test
    public void DeleteAllPictureTest() {
        Pages.loginPage().typeUsername();
        Pages.loginPage().typePassword();
        Pages.loginPage().clickToLogin();
        Pages.mainPage().clickOnMenu();
        Pages.mainPage().clickOnGalleryInMenu();
        Pages.galleryPage().clickOnMenuInGallery();
        Pages.galleryPage().clickOnMenuToDeleteAllImages();
        Espresso.onView(Pages.galleryPage().getImagesView()).check(new RecyclerViewItemCountAssertion(0));
    }
}
