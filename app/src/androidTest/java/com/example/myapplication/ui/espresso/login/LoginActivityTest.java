package com.example.myapplication.ui.espresso.login;

import com.example.myapplication.pages.Pages;
import com.example.myapplication.ui.login.LoginActivity;
import com.example.myapplication.utils.Constants;

import androidx.test.espresso.Espresso;
import androidx.test.ext.junit.rules.ActivityScenarioRule;

import org.junit.Before;
import org.junit.Rule;

import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import org.junit.Test;

public class LoginActivityTest {


    @Rule
    public ActivityScenarioRule<LoginActivity> mActivityRule = new ActivityScenarioRule<>(
            LoginActivity.class);


    @Before
    public void setUp() {

    }

    @Test
    public void testOnCreate1() {
        Pages.loginPage().typeUsername();
        Pages.loginPage().typePassword();
        Pages.loginPage().clickToLogin();
        Espresso.onView(Pages.mainPage().getTextHome()).check(matches(withText(Constants.SET_MESSAGE_IN_HOME_PAGE)));
    }
}