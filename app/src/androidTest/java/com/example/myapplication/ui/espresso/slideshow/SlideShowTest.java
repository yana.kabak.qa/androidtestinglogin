package com.example.myapplication.ui.espresso.slideshow;

import com.example.myapplication.pages.Pages;
import com.example.myapplication.ui.login.LoginActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import androidx.test.espresso.Espresso;
import androidx.test.ext.junit.rules.ActivityScenarioRule;

import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;

public class SlideShowTest {
    @Rule
    public ActivityScenarioRule<LoginActivity> mActivityRule = new ActivityScenarioRule<>(
            LoginActivity.class);


    @Before
    public void setUp() {
    }

    @Test
    public void testOnSwipeDisplay() {
        Pages.loginPage().typeUsername();
        Pages.loginPage().typePassword();
        Pages.loginPage().clickToLogin();
        Pages.mainPage().clickOnMenu();
        Pages.mainPage().clickOnSlideshowInMenu();
        Pages.slideshowPage().swipeLeft();
        Espresso.onView(Pages.slideshowPage().getImageView()).check(matches(isDisplayed()));

    }
}
