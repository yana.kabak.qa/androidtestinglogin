package com.example.myapplication.ui.espresso.settings;

import com.example.myapplication.pages.Pages;
import com.example.myapplication.ui.login.LoginActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import androidx.test.espresso.Espresso;
import androidx.test.espresso.assertion.ViewAssertions;
import androidx.test.ext.junit.rules.ActivityScenarioRule;

public class SettingsSetMessageTest {
    @Rule
    public ActivityScenarioRule<LoginActivity> mActivityRule = new ActivityScenarioRule<>(
            LoginActivity.class);


    @Before
    public void setUp() {
    }

    @Test
    public void testOnSetMessageInSetting() {
        Pages.loginPage().typeUsername();
        Pages.loginPage().typePassword();
        Pages.loginPage().clickToLogin();
        Pages.mainPage().clickOnToolMenu();
        Pages.mainPage().clickOnSettingsInToolMenu();
        Pages.settingsPage().clickOnFieldToTypeText();
        Pages.settingsPage().clearTextAreaBeforeTyping();
        Pages.settingsPage().typeText();
        Pages.settingsPage().clickOnButtonToSaveTypedText();
        Espresso.onView(Pages.settingsPage().getTextAreaContentInSetting()).check(ViewAssertions.matches(Pages.settingsPage().getTextAreaContentInSetting()));
    }
}
