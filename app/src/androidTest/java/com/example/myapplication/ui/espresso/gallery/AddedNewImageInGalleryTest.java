package com.example.myapplication.ui.espresso.gallery;

import android.os.SystemClock;

import com.example.myapplication.pages.Pages;
import com.example.myapplication.utils.RecyclerViewItemCountAssertion;
import com.example.myapplication.ui.login.LoginActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import androidx.recyclerview.widget.RecyclerView;
import androidx.test.espresso.Espresso;
import androidx.test.ext.junit.rules.ActivityScenarioRule;

public class AddedNewImageInGalleryTest {

    @Rule
    public ActivityScenarioRule<LoginActivity> mActivityRule = new ActivityScenarioRule<>(
            LoginActivity.class);

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;

    @Before
    public void setUp() {
    }

    @Test
    public void creatingPictureTest() {
        Pages.loginPage().typeUsername();
        Pages.loginPage().typePassword();
        Pages.loginPage().clickToLogin();
        Pages.mainPage().clickOnMenu();
        Pages.mainPage().clickOnGalleryInMenu();
        Pages.galleryPage().clickOnButtonToAddNewImage();
        SystemClock.sleep(4000);
        recyclerView = Pages.galleryPage().getCurrentView();
        adapter = recyclerView.getAdapter();
        Espresso.onView(Pages.galleryPage().getImagesView()).check(new RecyclerViewItemCountAssertion(adapter.getItemCount()));
    }
}
