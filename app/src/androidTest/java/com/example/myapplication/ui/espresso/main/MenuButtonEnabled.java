package com.example.myapplication.ui.espresso.main;

import com.example.myapplication.pages.Pages;
import com.example.myapplication.ui.login.LoginActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import androidx.test.espresso.Espresso;
import androidx.test.ext.junit.rules.ActivityScenarioRule;

import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isEnabled;

public class MenuButtonEnabled {

    @Rule
    public ActivityScenarioRule<LoginActivity> mActivityRule = new ActivityScenarioRule<>(
            LoginActivity.class);

    @Before
    public void setUp() {
    }

    @Test
    public void homeButtonEnabledTest() {
        Pages.loginPage().typeUsername();
        Pages.loginPage().typePassword();
        Pages.loginPage().clickToLogin();
        Pages.mainPage().clickOnMenu();
        Pages.mainPage().clickOnHomeInMenu();
        Espresso.onView(Pages.mainPage().getHomeButtonView()).check(matches(isEnabled()));
    }
}
