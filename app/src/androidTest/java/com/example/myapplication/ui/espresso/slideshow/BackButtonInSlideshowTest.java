package com.example.myapplication.ui.espresso.slideshow;

import com.example.myapplication.pages.Pages;
import com.example.myapplication.ui.login.LoginActivity;
import com.example.myapplication.utils.Constants;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import androidx.test.espresso.Espresso;
import androidx.test.ext.junit.rules.ActivityScenarioRule;

import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

public class BackButtonInSlideshowTest {
    @Rule
    public ActivityScenarioRule<LoginActivity> mActivityRule = new ActivityScenarioRule<>(
            LoginActivity.class);


    @Before
    public void setUp() {
    }

    @Test
    public void backButtonTest() {
        Pages.loginPage().typeUsername();
        Pages.loginPage().typePassword();
        Pages.loginPage().clickToLogin();
        Pages.mainPage().clickOnMenu();
        Pages.mainPage().clickOnSlideshowInMenu();
        Pages.slideshowPage().clickOnBackButton();
        Espresso.onView(Pages.mainPage().getTextHome()).check(matches(withText(Constants.SET_MESSAGE_IN_HOME_PAGE)));
    }
}
