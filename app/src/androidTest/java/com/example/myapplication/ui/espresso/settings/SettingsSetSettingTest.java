package com.example.myapplication.ui.espresso.settings;

import com.example.myapplication.pages.Pages;
import com.example.myapplication.ui.login.LoginActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import androidx.test.espresso.Espresso;
import androidx.test.ext.junit.rules.ActivityScenarioRule;

import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isEnabled;

public class SettingsSetSettingTest {
    @Rule
    public ActivityScenarioRule<LoginActivity> mActivityRule = new ActivityScenarioRule<>(
            LoginActivity.class);

    @Before
    public void setUp() {
    }

    @Test
    public void testOnSetSettings() {
        Pages.loginPage().typeUsername();
        Pages.loginPage().typePassword();
        Pages.loginPage().clickToLogin();
        Pages.mainPage().clickOnToolMenu();
        Pages.mainPage().clickOnSettingsInToolMenu();
        Pages.settingsPage().clickOnButtonThatShouldBeEnableInSettings();
        Espresso.onView(Pages.settingsPage().getButtonThatShouldEnableAfterClickOnFirstButton()).check(matches(isEnabled()));
    }
}
