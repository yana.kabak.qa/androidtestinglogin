package com.example.myapplication.pages;

import android.widget.Button;

import com.example.myapplication.R;
import com.example.myapplication.ui.login.LoginActivity;
import com.example.myapplication.utils.Constants;

import androidx.test.espresso.Espresso;
import androidx.test.espresso.action.ViewActions;

import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

public class LoginPage {

    private int username = R.id.username;
    private int password = R.id.password;
    private int loginButton = R.id.login;

    public void typeUsername(){
        Espresso.onView(withId(username)).perform(ViewActions.typeText(Constants.USERNAME));
    }

    public void typePassword(){
        Espresso.onView(withId(password)).perform(ViewActions.typeText(Constants.PASSWORD));
    }

    public void clickToLogin(){
        Espresso.onView(withId(loginButton)).perform(click());
    }

    public Button getLoginButton(LoginActivity loginActivity){
        return loginActivity.findViewById(R.id.login);
    }

    public void clickOnLoginButton(Button button){
        button.performClick();
    }
}
