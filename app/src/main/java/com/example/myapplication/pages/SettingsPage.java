package com.example.myapplication.pages;

import com.example.myapplication.R;
import com.example.myapplication.utils.Constants;

import org.hamcrest.Matcher;
import org.robolectric.shadows.ShadowActivity;

import androidx.test.espresso.Espresso;
import androidx.test.espresso.action.ViewActions;
import androidx.test.espresso.matcher.ViewMatchers;

import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

public class SettingsPage {
    private int settingFieldForText = R.string.signature_title;
    private int fieldTypeText = android.R.id.edit;
    private int saveButton = android.R.id.button1;
    private int settingFieldWithEnableButton = R.string.sync_title;
    private int settingFieldWithButtonThatShouldCheckEnable = R.string.attachment_title;
    private int backButton = android.R.id.home;

    public void clickOnFieldToTypeText(){
        Espresso.onView(withText(settingFieldForText)).perform(click());
    }

    public void clearTextAreaBeforeTyping(){
        Espresso.onView(ViewMatchers.withId(fieldTypeText)).perform(ViewActions.clearText());
    }

    public void typeText(){
        Espresso.onView(ViewMatchers.withId(fieldTypeText)).perform(ViewActions.typeText(Constants.HELLO));
    }

    public void clickOnButtonToSaveTypedText(){
        Espresso.onView(ViewMatchers.withId(saveButton)).perform(click());
    }

    public Matcher getTextAreaContentInSetting(){
        return withText(Constants.HELLO);
    }

    public void clickOnButtonThatShouldBeEnableInSettings(){
        Espresso.onView(ViewMatchers.withText(settingFieldWithEnableButton)).perform(click());
    }

    public Matcher getButtonThatShouldEnableAfterClickOnFirstButton(){
        return withText(settingFieldWithButtonThatShouldCheckEnable);
    }

    public void clickOnBackButton(ShadowActivity shadowActivity){shadowActivity.clickMenuItem(backButton);}
}
