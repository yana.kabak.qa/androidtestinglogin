package com.example.myapplication.pages;

import android.view.View;

import com.example.myapplication.R;
import com.example.myapplication.ui.gallery.ScrollingActivity;
import com.example.myapplication.utils.Constants;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.hamcrest.Matcher;
import org.robolectric.shadows.ShadowActivity;

import androidx.recyclerview.widget.RecyclerView;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.espresso.Espresso;
import androidx.test.espresso.matcher.ViewMatchers;

import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static com.example.myapplication.utils.CurrentActivity.getActivity;

public class GalleryPage {
    private int addNewImageButton = R.id.fab;
    private int imageView = R.id.main_fragment;
    private int backButton = android.R.id.home;

    public void clickOnButtonToAddNewImage(){
        Espresso.onView(withId(addNewImageButton)).perform(click());
    }

    public RecyclerView getCurrentView(){
        return getActivity().findViewById(imageView);
    }

    public Matcher getImagesView(){
        return withId(imageView);
    }

    public void clickOnMenuInGallery(){
        Espresso.openActionBarOverflowOrOptionsMenu(ApplicationProvider.getApplicationContext());
    }

    public void clickOnMenuToDeleteAllImages(){
        Espresso.onView(ViewMatchers.withText(Constants.DELETE_ALL)).perform(click());
    }

    public View getButtonActivity(ScrollingActivity scrollingActivity){
        return scrollingActivity.findViewById(addNewImageButton);
    }

    public void clickOnBackButton(ShadowActivity shadowActivity){
        shadowActivity.clickMenuItem(backButton);
    }

    public void clickOnNewImageButton(FloatingActionButton floatingActionButton){
        floatingActionButton.performClick();
    }
}
