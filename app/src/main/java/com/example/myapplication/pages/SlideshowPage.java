package com.example.myapplication.pages;

import com.example.myapplication.R;

import org.hamcrest.Matcher;
import org.robolectric.shadows.ShadowActivity;

import androidx.appcompat.widget.AppCompatImageButton;
import androidx.test.espresso.Espresso;
import androidx.test.espresso.action.ViewActions;

import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.instanceOf;

public class SlideshowPage {

    private int imageArea = R.id.viewPage;
    private int backButton = android.R.id.home;

    public void clickOnBackButton(){
        Espresso.onView(allOf(instanceOf(AppCompatImageButton.class))).perform(click());
    }

    public void swipeLeft(){
        Espresso.onView(withId(imageArea)).perform(ViewActions.swipeLeft());
    }

    public Matcher getImageView(){
        return withId(R.id.viewPage);
    }

    public void clickOnBackButton(ShadowActivity shadowActivity){shadowActivity.clickMenuItem(backButton);}
}
