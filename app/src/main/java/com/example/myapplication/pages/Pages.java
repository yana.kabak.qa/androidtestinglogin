package com.example.myapplication.pages;

public class Pages {

    private static LoginPage loginPage;
    private static MainPage mainPage;
    private static GalleryPage galleryPage;
    private static SettingsPage settingsPage;
    private static SlideshowPage slideshowPage;

    public static LoginPage loginPage() {
        if (loginPage == null) {
            loginPage = new LoginPage();
        }
        return loginPage;
    }

    public static MainPage mainPage() {
        if (mainPage == null) {
            mainPage = new MainPage();
        }
        return mainPage;
    }

    public static GalleryPage galleryPage() {
        if (galleryPage == null) {
            galleryPage = new GalleryPage();
        }
        return galleryPage;
    }

    public static SettingsPage settingsPage() {
        if (settingsPage == null) {
            settingsPage = new SettingsPage();
        }
        return settingsPage;
    }

    public static SlideshowPage slideshowPage() {
        if (slideshowPage == null) {
            slideshowPage = new SlideshowPage();
        }
        return slideshowPage;
    }
}
