package com.example.myapplication.pages;

import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.ui.main.MainActivity;
import com.example.myapplication.utils.Constants;

import org.hamcrest.Matcher;
import org.robolectric.shadows.ShadowActivity;

import androidx.appcompat.widget.AppCompatImageButton;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.espresso.Espresso;

import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.instanceOf;

public class MainPage {
    private int toolbar = R.id.toolbar;
    private int galleryButton = R.id.nav_gallery;
    private int avatarView = R.id.imageView;
    private int homeButton = R.id.nav_home;
    private int slideshowButton = R.id.nav_slideshow;
    private int textOnHomePage = R.id.text_home;
    private int settingButton = R.id.action_settings;

    public Matcher getTextHome(){
        return withId(textOnHomePage);
    }

    public void clickOnMenu(){ Espresso.onView(allOf(instanceOf(AppCompatImageButton.class), withParent(withId(toolbar)))).perform(click()); }

    public void clickOnGalleryInMenu(){ Espresso.onView(withId(galleryButton)).perform(click()); }

    public Matcher getAvatarView(){ return withId(avatarView); }

    public void clickOnHomeInMenu(){ Espresso.onView(withId(homeButton)).perform(click()); }

    public Matcher getHomeButtonView(){ return withId(homeButton); }

    public void clickOnToolMenu(){ Espresso.openActionBarOverflowOrOptionsMenu(ApplicationProvider.getApplicationContext()); }

    public void clickOnSettingsInToolMenu(){ Espresso.onView(withText(Constants.SETTINGS)).perform(click());}

    public void clickOnSlideshowInMenu(){ Espresso.onView(withId(slideshowButton)).perform(click()); }

    public void clickOnMenuItemSetting(ShadowActivity shadowActivity){ shadowActivity.clickMenuItem(settingButton);}

    public TextView getWelcomeText(MainActivity mainActivity){ return mainActivity.findViewById(textOnHomePage);}
}
