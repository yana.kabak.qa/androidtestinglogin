package com.example.myapplication.ui.gallery;

import android.net.Uri;

class PictureItem {
    public Uri uri;
    public String date;
}
