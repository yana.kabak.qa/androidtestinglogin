package com.example.myapplication.ui.home;

import com.example.myapplication.utils.Constants;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class HomeViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public HomeViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue(Constants.SET_MESSAGE_IN_HOME_PAGE);
    }

    public LiveData<String> getText() {
        return mText;
    }
}