package com.example.myapplication.utils;

public class Constants {
    public static String USERNAME = "jane@gmail.com";
    public static String PASSWORD = "123456";
    public static String NAME = "Jane Doe";
    public static String ERROR_LOGIN = "Error logging in";
    public static String SET_MESSAGE_IN_HOME_PAGE = "Welcome! Jane Doe";
    public static String DELETE_ALL = "Delete all";
    public static String SETTINGS = "Settings";
    public static String HELLO = "hello";
    public static String FAILED_LOGIN = "Login failed";

}
