package com.example.myapplication.data;

import com.example.myapplication.data.model.LoggedInUser;
import com.example.myapplication.utils.Constants;

import java.io.IOException;

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
public class LoginDataSource {

    public Result<LoggedInUser> login(String username, String password) {
        try {
            if(username.equals(Constants.USERNAME) && password.equals(Constants.PASSWORD)) {
                LoggedInUser fakeUser =
                        new LoggedInUser(
                                java.util.UUID.randomUUID().toString(),
                                Constants.NAME);
                return new Result.Success<>(fakeUser);
            }else {
                return new Result.Error(new IOException(Constants.ERROR_LOGIN));
            }
        } catch (Exception e) {
            return new Result.Error(new IOException(Constants.ERROR_LOGIN, e));
        }
    }

    public void logout() {
        // TODO: revoke authentication
    }
}